
package com.example.profiling.contoller;

import com.example.profiling.Exception.ApiException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ClientControllerTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    ClientController clientController;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }
    @Test
    public void testForOkStatus(){
        Mockito.when(restTemplate.getForEntity("http://localhost:8080/profiles", String.class))
                .thenReturn(ResponseEntity.ok("local"));
        String response =clientController.getRemoteProfile();
        Assertions.assertEquals("local",response);

    }

    @Test
    public void testFor404Status(){
        Mockito.when(restTemplate.getForEntity("http://localhost:8080/profiles", String.class))
                .thenReturn(new ResponseEntity<String>(HttpStatus.NOT_FOUND));
        ApiException e =  Assertions.assertThrows(ApiException.class, ()->clientController.getRemoteProfile());
        Assertions.assertEquals("Server not reachable", e.getMessage());
    }


    @Test
    public void testFor500Status(){
        Mockito.when(restTemplate.getForEntity("http://localhost:8080/profiles", String.class))
                .thenReturn(new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR));
        ApiException e =  Assertions.assertThrows(ApiException.class, ()->clientController.getRemoteProfile());
        Assertions.assertEquals("Error in remote service", e.getMessage());
    }

}
