package com.example.profiling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class ProfilingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProfilingApplication.class, args);
	}

	@Bean
	public RestTemplate getBean(){
		return new RestTemplate();
	}

}
