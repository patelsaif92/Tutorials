package com.example.profiling.contoller;

import com.example.profiling.Exception.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/client")
public class ClientController {
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/profile")
    public String getRemoteProfile(){


            ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:8080/profiles", String.class);
            if(HttpStatus.OK.equals(response.getStatusCode())) {
                return "The selected profile is :" + response.getBody();
            }else if(HttpStatus.NOT_FOUND.equals(response.getStatusCode())){
                throw new ApiException("Server not reachable");
            }else if(HttpStatus.INTERNAL_SERVER_ERROR.equals(response.getStatusCode())){
                throw new ApiException("Error in remote service");
            }

        return "error";
    }
}
