package com.example.profiling.contoller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Value("${environment_name}")
    private String env;

    @GetMapping("/profiles")
    public String getProfile(){
        return env;
    }
}
